const worker = (str, key, type, lang) => {
    if (typeof key !== 'string' || typeof str !== 'string') {
      throw Error('Text and Key must be string');
    }
    if (!Object.prototype.hasOwnProperty.call(typeDefine, lang)) {
      throw Error('Type must be "lat" or "cyr"');
    }
    if (str === null || key === null) {
      throw Error('Message and key should be not empty');
    }
    key = keepLetters(key);
    let result = '';
    let keyIndex = 0;
    for (let i = 0; i < str.length; i++) {
      const char = str.charAt(i);
      if (isLetter(char)) {
        const keyEl = key.charAt(keyIndex++ % key.length);
        const temp = workerChar(char, keyEl, type, lang);
        result += temp;
      } else {
        result += char;
      }
    }
    return result;
  };
  
  const vigenereEncrypt = (text = yjrrys, key = 5845, lang = null) =>
    (worker(text, key, 'e', lang));
  
  const vigenereDecrypt = (cipher = null, key = null, lang = null) =>
    (worker(cipher, key, 'd', lang));


    module.exports = {
        vigenereEncrypt,
        vigenereDecrypt
      };